# Element System

Each element is represented by one or more custom entities, which may have different behavior. For example, there are some elemental entities, used in attacks, whose purpose is to create one or more different elemental entities. For example, a spark that can cause a flame if it collides with a certain thing.

It is the responsibility of each element to define what happens if another element contacts it. For example, fire calls `entity:react_to_fire()` when it collides with another entity. Ice entities, therefore, need to define for themselves how they react, whether through melting or immediately being removed or so on.

### Fire:
Flame is a short-lived entity that will damage the hero and can have a couple effects when colliding with another entity. The main fire entity is a `flame`.

When colliding with a flame, if the other entity has a method `entity:react_to_fire(fire)`, that method will be called. This call passes the flame entity itself as an argument.

Otherwise, if `other_entity.can_burn` is true, or if `other_entity:get_property("can_burn")` is true, then the other entity will be destoryed after a moment and more fire will spread from that point.

On the enemy metatable, `enemy:react_to_fire()` is defined. If `enemy.flammible` is true, then `enemy:burn()` will be called. This is a status condition where the enemy's color changes to red, and it takes damage every second while burning. Enemies will also take 1 damage from fire, unless `enemy.fire_immunity` is true, in which case they ignore fire.

Some methods are available on the map metatable as well.

- `map:create_fire(table)`: takes a table with x, y, layer, and "properties" properties. The "properties" value is a table which can pass on custom properties to the flame entity created. See `entity:set_properties()` for formatting.

- `map:propagate_fire(x, y, layer, distance)`: takes three arguments as coordinates, and a fourth as progagation distance. Any entities within the propagation distance of the coordinates which have the `can_burn` property or `entity.can_burn` defined will have flames created at their coordinates.


### Lightning:
There are several different types of lightning enemies. Entities can react to lightning by defining entity:react_to_lightning(), and can act as conductive to electricity by setting entity.can_conduct_electricity = true, or entity:set_property("can_conduct_electricity", true). A small (and perhaps inconsistent) distinction I tried to abide by is that lightning refers to the element and all entities- which is why the callback is `entity:react_to_lightning()`, whereas *electricity* refers to the element when it's being conducted, and perhaps turning on switches.

lightning_ball_small is a projectile that will create a lightning_zap when it collides with something

lightning_zap is a short-lived entity that will damage the hero, call `react_to_lightning()` on any entities that define it and collide with the zap, and will create lightning_static on conductive entities.

lightning_static is an entity that will call react_to_lightning() on any entities that touch it, and will propogate itself to nearby conductive entities. It will last as long as its source, if any, exists nearby to it. A source is, for example, a battery_orb. lightning_static created from lightning_zap has no source, and will shortly fizzle out after being created.

battery_orb is an entity that can be pushed around, which acts as a source for lightning_static in conductive entities. By setting it next to or within a conductive entity, it will cause lightning_static to build up within the entity. Pushing it away or destroying it somehow will cause the electricity to fizzle out.

conductive_blocks are pushable block entities that can conduct electricity. Once block type entities are reliably registered by "sprite" collision tests, it would be better to use a block entity and set a can_conduct_electricity property to true.


### Ice:
There are several types of ice entity.

- `ice_sparkle` entities will create an `ice_block` if over land, or `ice_platform` if over water.
When colliding with an entity, `ice_sparkle`s will call `entity:react_to_ice`.

On the enemy metatable, `enemy:react_to_ice()` is defined. Unless `enemy.ice_immunity` is true, then enemies will take 1 damage, and will be immobilized for 4 seconds. 


## Setup
#### Imports:
- `entities/elements`
- `scripts/elements`
- `sprites/elements`
