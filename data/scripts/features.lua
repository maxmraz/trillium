-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled or disabled independently by commenting
-- or uncommenting lines below.

local features = {}
--Scripts that need to be called first:
require"scripts/multi_events"
require"scripts/utility/item_name_retriever" --this one has to be near the top for other scripts to use it

--Scripts that can be called later:
require("items/charms/charm_manager")
require"scripts/action/dash_manager"
require"scripts/action/explosives"
require"scripts/action/fall_manager"
require"scripts/action/hole_drop_landing"
require"scripts/action/swim_manager"
require"scripts/checkpoint/checkpoint_actions"
require"scripts/checkpoint/enemy_respawn_manager"
require"scripts/elements/enemy_elemental_meta"
require"scripts/elements/hero_elemental_meta"
require"scripts/elements/map_elemental_meta"
require"scripts/fx/fog"
require"scripts/fx/lighting/lighting_manager"
require"scripts/fx/lighting/map_lighting"
require"scripts/fx/white_flash"
require"scripts/gameover"
require"scripts/hud/hud"
require"scripts/hud/title_slam"
--require"scripts/menus/dialog_box"
require"scripts/menus/inventory/pause_menu"
require"scripts/menus/shop/shop_manager"
require"scripts/meta/bush"
require"scripts/meta/camera"
require"scripts/meta/custom_entity"
require"scripts/meta/enemy"
require"scripts/meta/game"
require"scripts/meta/hero"
require"scripts/meta/item"
require"scripts/meta/map"
require"scripts/meta/switch"
require("scripts/misc/magic_regeneration_manager")
--require"scripts/misc/solid_ground_manager"
require"scripts/status_effects/enemy_status_manager"
require"scripts/status_effects/status_manager"
require"scripts/trillium_dialog/dialog_manager"
require"scripts/utility/draw_bounding_boxes"
require("scripts/utility/ammo_tracker").add_to_item_api()
require"scripts/utility/angle_utility"
require"scripts/utility/draw_bounding_boxes"
require"scripts/utility/savegame_tables"
require"scripts/utility/table_duplication"
require"scripts/utility/text_stamper"
require"scripts/weather/weather_manager"


--Some scripts need a game passes to initialize. features.init(game) is called by the game manager, so those scripts are initialized here:
function features.init(game)
  require("items/materials/materials_manager"):init(game)
  require("items/meal_manager"):init(game)
  require("scripts/button_inputs"):initialize(game)
  require("scripts/debug_keys"):initialize(game)
  --require("scripts/fx/lighting_effects"):initialize()
end

return features
