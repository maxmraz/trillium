local entity_meta = sol.main.get_metatable"custom_entity"

function entity_meta:is_on_screen()
  local entity = self
  local map = entity:get_map()
  local camera = map:get_camera()
  local camx, camy = camera:get_position()
  local camwi, camhi = camera:get_size()
  local entityx, entityy = entity:get_position()

  local on_screen = entityx >= camx and entityx <= (camx + camwi) and entityy >= camy and entityy <= (camy + camhi)
  return on_screen
end

