--[[
This is a tall and comparatively narrow box -- it's useful for situations where the player is being asked a question with several answer choices (more than would fit in a normal dialog)
--]]

return {
  height = 160,
  width = 256,
  visible_lines = 9,
  origin_y = 48,
  origin_x = 16,
}