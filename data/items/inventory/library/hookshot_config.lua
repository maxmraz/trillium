
local config = {
  --hookshot range in px:
  distance = 160,

  --hookshot speed (in px/s):
  speed = 350,

  --amount of time (in ms) after the hookstart starts retracting that the rope goes slack (this is just an asthetic choice.
  slack_delay = 120,

  --Entity types which can be hooked, pulling the hero to them.
  --Additionally, any entity with the property "hookshot_target" or where entity.hookshot_target == true
  --or with a method entity:is_hookable() which returns true will also be hookable.
  hookable_entity_types = {
    "chest",
    "block",
  },

  --Entities of these types, with these sprites, will be hookable as well. This is useful if you want some destructables to be hookable, for example
  hookable_by_animation_set = {
    ["destructible"] =
      {
        "destructibles/bush", 
        "destructibles/pumpkin_small",
      },
    ["custom_entity"] =
      {
        "foliage/hookseed",
      },
  },

  --Entity types which can be snagged and retrieved with the hookshot. 
  --Additionally, any entity with entity.hookshot_catchable == true, or with the property "hookshot_catchable",
  --or with the method entity:is_catchable_with_hookshot() which returns true, will also be retrievable
  catchable_entity_types = {
    "pickable",
  },


}

return config