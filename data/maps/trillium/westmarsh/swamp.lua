local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  --Temp entities for spacing while making map
  for e in map:get_entities("grid_ref") do
    e:remove()
  end

  map:set_fog"fog"
  game:set_world_rain_mode(map:get_world(), "rain")
end)
